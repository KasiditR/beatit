using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController),typeof(PlayerCharacter))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController _characterController = null;
    [SerializeField] private PlayerCharacter _playerCharacter = null;
    private float _gravity = -9.81f;


    private void Awake() 
    {
        _characterController = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        Vector3 moveDiction = new Vector3(Input.GetAxis(Tags.horizontalAxis) * _playerCharacter.Speed, _gravity, _playerCharacter.Speed);
        _characterController.Move(moveDiction * Time.deltaTime);
        if(_playerCharacter.Speed < 20)
        {
            _playerCharacter.Speed += 2 * Time.deltaTime;
        }
    }
}
