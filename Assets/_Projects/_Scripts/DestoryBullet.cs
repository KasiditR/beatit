﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoryBullet : MonoBehaviour
{
    [SerializeField] private float timeCount;
    [SerializeField] private float timeDestory;
    private void FixedUpdate()
    {
        TimeDestory();
    }
    private void TimeDestory()
    {
        timeCount += Time.deltaTime;
        if(timeCount >= timeDestory)
        {
            Destroy(gameObject);
            timeCount = 0;
        }
    }
}
