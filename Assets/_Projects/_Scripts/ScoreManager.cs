﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreGameOverText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI highScore;
    [SerializeField] private float scoreCount;
    [SerializeField] private float highScoreCount;
    [SerializeField] private float pointPerScond;
    public bool scoreInreasing;
    private void Start()
    {
        if(PlayerPrefs.HasKey("HighScore"))
        {
            highScoreCount = PlayerPrefs.GetFloat("HighScore");
        }
    }
    public void Update()
    {
        if (scoreInreasing)
        {
            scoreCount += pointPerScond * Time.deltaTime;
        }
        if(scoreCount > highScoreCount)
        {
            highScoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScore", highScoreCount);
        }
        scoreText.text = $"Score: {Mathf.Round(scoreCount)}";
        scoreGameOverText.text = $"Score: {Mathf.Round(scoreCount)}";
        highScore.text = $"HighScore: {Mathf.Round(highScoreCount)}";
    }
}
