using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCharacter : MonoBehaviour
{
    [SerializeField] private HealthBar _healthBar;

    public HealthBar HealthBar { get => _healthBar; set => _healthBar = value; }
    protected int health { get; set; }
    protected float speed { get; set; }
    protected int coin { get; set; }
    protected virtual void Init(int health, float speed)
    {
        this.health = health;
        this.speed = speed;
        _healthBar.SetMaxHealth(this.health);
    }
    
}
