using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : BaseCharacter,IDamageable,ICoinable,IHealable
{
    [SerializeField] private Animator _anim = null;
    [SerializeField] private int _health = 100;
    [SerializeField] private float _speed = 10f;

    public float Speed { get => _speed; set => _speed = value; }

    public event Action onDie;
    public event Action onGetCoin;
    public event Action onHeal;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        Init(_health,_speed);
    }
    public void Die()
    {
        _anim.SetBool("IsDeath",true);
        onDie?.Invoke();
    }
    public void InitPlayer(int health, float speed)
    {
        base.Init(health, speed);
    }

    public void TakeDamege(int damage)
    {
        health -= damage;
        _speed -= damage;
        HealthBar.SetHealth(health);
        if (health > 0)
        {
            return;
        }
        Die();
    }

    public void TakeCoin(int takecoin)
    {
        coin += takecoin;
        onGetCoin?.Invoke();
    }

    public void Heal(int heal)
    {
        health += heal;
        HealthBar.SetHealth(health);
        onHeal?.Invoke();
    }
}
