﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTriggerDamage : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == Tags.player)
        {
            // this.gameObject.GetComponent<>
            var damage = other.gameObject.GetComponent<IDamageable>();
            damage?.TakeDamege(5);
            Debug.Log("Damage");
            // damage.Move(5);
        }
    }
}
