﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTriggerCoin : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == Tags.player)
        {
            var coin = other.gameObject.GetComponent<ICoinable>();
            coin?.TakeCoin(1);
        }
    }
}
