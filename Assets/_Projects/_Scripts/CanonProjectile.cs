﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CanonProjectile : MonoBehaviour
{
    [SerializeField] private Transform shootPoint;
    [SerializeField] private float lookRadius;
    [SerializeField] private Rigidbody bullet;
    [SerializeField] private GameObject target;
    [SerializeField] private float coolDown;
    [SerializeField] private float timeCount;
    void Update()
    {
        Shoot();
    }
    private void Shoot()
    {
        timeCount += Time.deltaTime;
        Vector3 vo = CalculateVelocity(target.transform.position, shootPoint.position, 1f);
        if(timeCount >= coolDown)
        {
            Rigidbody fire = Instantiate(bullet, shootPoint.position, Quaternion.identity);
            fire.velocity = vo;
            timeCount = 0;
        }
    }
    private Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        //define the distance x and y first
        Vector3 distance = target - origin;
        Vector3 distance_x_z = distance;
        distance_x_z.Normalize();
        distance_x_z.y = 0;

        //creating a float that represents our distance 
        float sy = distance.y;
        float sxz = distance.magnitude;


        //calculating initial x velocity
        //Vx = x / t
        float Vxz = sxz / time;

        ////calculating initial y velocity
        //Vy0 = y/t + 1/2 * g * t
        float Vy = sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distance_x_z * Vxz;
        result.y = Vy;



        return result;
    }
}
