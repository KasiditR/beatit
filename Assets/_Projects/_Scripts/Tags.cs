﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags
{
    public const string verticalAxis = "Vertical";
    public const string horizontalAxis = "Horizontal";
    public const string player = "Player";
}
