﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManger : MonoBehaviour
{
    [SerializeField] private Button quitButton;
    [SerializeField] private Button restartButton;
    // [SerializeField] private Animator anim;
    [SerializeField] private HealthBar healthBar;
    // [SerializeField] private CharacterController charContorll;
    [SerializeField] private TextMeshProUGUI getCoin;
    [SerializeField] private RectTransform dialogGameOver;
    // [SerializeField] private float speed = 20;
    // [SerializeField] private int maxHealth = 5;
    [SerializeField] private int currentHealth = 0;
    private ScoreManager scoreManager;
    // private Vector3 moveDiction;
    // private float gravityPlayer =-9.81f;
    private int isDeathHash;
    private int totalCoin = 0;
    
    private void Start()
    {
        // anim = GetComponent<Animator>();
        // charContorll = GetComponent<CharacterController>();
        isDeathHash = Animator.StringToHash("isDeath");
        quitButton.onClick.AddListener(QuitButton);
        restartButton.onClick.AddListener(RestartButton);
        scoreManager = FindObjectOfType<ScoreManager>();
        // currentHealth = maxHealth;
        // healthBar.SetMaxHealth(maxHealth);
    }
    private void Update()
    {
        // Move(0);
        // if(currentHealth == 0)
        // {
        //     IsDeath();
        //     scoreManager.scoreInreasing = false;
        //     dialogGameOver.gameObject.SetActive(true);
        // }
        // if (currentHealth > maxHealth)
        // {
        //     currentHealth = maxHealth;
        // }
    }
    private void IsDeath()
    {
        // speed = 0;
        // anim.SetBool(isDeathHash, true);
    }
    private void RestartButton()
    {
        SceneManager.LoadScene("Game");
        dialogGameOver.gameObject.SetActive(false);
    }
    
    private void QuitButton()
    {
        Application.Quit();
    }
    // public void Move(int slow )
    // {
    //     speed -= slow;
    //     moveDiction = new Vector3(Input.GetAxis(Tags.horizontalAxis) * speed, gravityPlayer, speed);
    //     charContorll.Move(moveDiction * Time.deltaTime);
    //     if(speed < 20)
    //     {
    //         speed += 2 * Time.deltaTime;
    //     }
    // }
    public void GetCoin(int coin)
    {
        totalCoin += coin;
        getCoin.text = $"Coin: {totalCoin}";
    }
    public void HealHeathBar(int heal)
    {
        currentHealth += heal;
        healthBar.SetHealth(currentHealth);
    }
    public void TakeDamege(int damege)
    {
        currentHealth -= damege;
        healthBar.SetHealth(currentHealth);
    }

}
