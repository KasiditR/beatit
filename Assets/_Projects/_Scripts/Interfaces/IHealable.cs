using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public interface IHealable 
{
    public event Action onHeal;
    public void Heal(int heal);
}