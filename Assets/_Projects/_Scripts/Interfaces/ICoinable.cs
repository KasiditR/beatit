using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public interface ICoinable 
{
    public event Action onGetCoin;
    public void TakeCoin(int coin);
}
