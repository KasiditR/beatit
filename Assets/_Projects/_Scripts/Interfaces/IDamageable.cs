using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IDamageable 
{
    public event Action onDie;
    public void TakeDamege(int damage);
    public void Die();
}