﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private GameObject[] tilePrefabs;
    [SerializeField] private float tileLenght;
    [SerializeField] private float rangeDestory;
    private List<GameObject> activeTile = new List<GameObject>();
    private float zSpawn = 0;
    private int numberOfTiles = 5;
    void Start()
    {
        for (int i = 0; i < numberOfTiles; i++) 
        {
           if(i == 0)
            {
                SpawnTile(0);
            }
            else
            {
                SpawnTile(Random.Range(0, tilePrefabs.Length));
            }
        }
    }
    void Update()
    {
        if (playerTransform.position.z - rangeDestory > zSpawn - (numberOfTiles * tileLenght)) 
        {
            SpawnTile(Random.Range(0, tilePrefabs.Length));
            DeleteTile();
        }
    }
    public void SpawnTile(int tileIndex)
    {
        GameObject go = Instantiate(tilePrefabs[tileIndex], transform.forward * zSpawn, transform.rotation);
        activeTile.Add(go);
        zSpawn += tileLenght;
    }
    private void DeleteTile()
    {
        Destroy(activeTile[0]);
        activeTile.RemoveAt(0);
    }
}
