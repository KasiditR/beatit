﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsTriggerHeal : MonoBehaviour
{
    [SerializeField] private int _healPoint;
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == Tags.player)
        {
            var healPlayer = other.GetComponent<IHealable>();
            // healPlayer?.Heal(_healPoint);
            healPlayer?.Heal(5);
            Destroy(gameObject);
        }
    }
}
