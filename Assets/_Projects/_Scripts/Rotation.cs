﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float augularSpeed;
    [SerializeField] protected Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void FixedUpdate()
    {
        OnRotation();
    }
    private void OnRotation()
    {
        speed = rb.velocity.magnitude;
        augularSpeed = rb.angularVelocity.magnitude * Mathf.Rad2Deg;
        var q = Quaternion.AngleAxis(45, Vector3.up);
        float angle;
        Vector3 axis;
        q.ToAngleAxis(out angle, out axis);
        rb.angularVelocity = axis * angle * Mathf.Deg2Rad;
    }
}
